/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

import dao.ProductoDAO;
import imp.ProductoDAOImp;
import java.util.List;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import model.Producto;

/**
 *
 * @author reyes
 */
@Named(value = "productoBean")
@ViewScoped
public class ProductoBean {

    private List<Producto> listaProducto;
    private Producto producto;
    
    public ProductoBean() {
    }

    public void setListaProducto(List<Producto> listaProducto) {
        this.listaProducto = listaProducto;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }
    
    public List<Producto> getListaProducto() {
        ProductoDAO pDAO = new ProductoDAOImp();
        listaProducto = pDAO.ListarProductos();
        return listaProducto;
    }
    
    public void prepararNuevoProducto()  {
        producto = new Producto();
    }
    
    public void nuevoProducto() {
        ProductoDAO pDAO = new ProductoDAOImp();
        pDAO.newProducto(producto);
    }
    
    public void modidicarProducto() {
        ProductoDAO pDAO = new ProductoDAOImp();
        pDAO.deleteProducto(producto);
        producto = new Producto();
    }
    
       public void eliminarProducto() {
        ProductoDAO pDAO = new ProductoDAOImp();
        pDAO.deleteProducto(producto);
        producto = new Producto();
    }
    
}
