/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;


import dao.VendedorDAO;
import imp.VendedorDAOImp;
import java.util.List;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import model.Vendedor;

/**
 *
 * @author reyes
 */
@Named(value = "vendedorBean")
@ViewScoped
public class VendedorBean {

    /**
     * Creates a new instance of VendedorBean
     */
    
    private List<Vendedor> listaVendedor;
    private Vendedor vendedor;
    
    public VendedorBean() {
        vendedor = new Vendedor();
    }

    public List<Vendedor> getListaVendedor() {
        VendedorDAO vDAO = new VendedorDAOImp();
        listaVendedor = vDAO.listarVendedores();
        return listaVendedor;
    }

    public void setListaVendedor(List<Vendedor> listaVendedor) {
        this.listaVendedor = listaVendedor;
    }

    public Vendedor getVendedor() {
        return vendedor;
    }

    public void setVendedor(Vendedor vendedor) {
        this.vendedor = vendedor;
    }
    
    public void prepararNuevoVendedor()  {
        vendedor = new Vendedor();
    }
    
    public void nuevoVendedor() {
        VendedorDAO vDAO = new VendedorDAOImp();
        vDAO.updateVendedor(vendedor);
    }
    
    public void modidicarVendedor() {
        VendedorDAO vDAO = new VendedorDAOImp();
        vDAO.deleteVendedor(vendedor);
        vendedor = new Vendedor();
    }
    
    public void eliminarVendedor() {
        VendedorDAO venDAO = new VendedorDAOImp();
        venDAO.deleteVendedor(vendedor);
        vendedor = new Vendedor();
    }
   
}
