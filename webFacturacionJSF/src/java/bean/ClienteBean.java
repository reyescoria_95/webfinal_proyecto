/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

import dao.ClienteDAO;
import imp.ClienteDAOImp;
import java.util.List;
import javax.inject.Named;
import javax.enterprise.context.Dependent;
import model.Cliente;

/**
 *
 * @author reyes
 */
@Named(value = "clienteBean")
@Dependent
public class ClienteBean {

    private List<Cliente> listaCliente;
    private Cliente cliente;
    
    public ClienteBean() {
    }

    public void setListaCliente(List<Cliente> listaCliente) {
        this.listaCliente = listaCliente;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
    
    public List<Cliente> getListaCliente() {
        ClienteDAO cDAO = new ClienteDAOImp();
        listaCliente = cDAO.ListarClientes();
        return listaCliente;
    }
    
    public void prepararNuevoCliente()  {
        cliente = new Cliente();
    }
    
    public void nuevoCliente() {
        ClienteDAO cDAO = new ClienteDAOImp();
        cDAO.newCliente(cliente);
    }
    
    public void modidicarCliente() {
        ClienteDAO cDAO = new ClienteDAOImp();
        cDAO.deleteCliente(cliente);
        cliente = new Cliente();
    }

    public void eliminarCliente() {
        ClienteDAO cDAO = new ClienteDAOImp();
        cDAO.deleteCliente(cliente);
        cliente = new Cliente();
    }
}
