/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imp;

import dao.VendedorDAO;
import java.util.List;
import javax.jms.Session;
import javax.transaction.Transaction;

/**
 *
 * @author reyes
 */
public class VendedorDAOImp implements VendedorDAO{

   @Override
    public List<Vendedor> ListarVendedores() {
        List<Vendedor> lista = null;
        Session session = HibernateUtil.getSessionfactory().openSession();
        Transaction t = session.beginTransaction();
        String hql="FROM Vendedor";
        
        try{
            lista = session.createQuery(hql).list();
            t.commit();
            session.close();
        }catch (Exception e) {
            System.out.println(e.getMessage());
            t.rollback();
        }
        return lista;
    }

    @Override
    public void newVendedor(Vendedor vendedor) {
        Session session=null;
        try{
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.save(Vendedor);
            session.getTransaction().commit();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            session.getTransaction().rollback();
        } finally {
            if(session != null) {
                session.close();
            }
        }
    }

    @Override
    public void updateVendedor(Vendedor vendedor) {
        Session session=null;
        try{
            session = HibernateUtil.getsessionFactory().openSession();
            session.beginTransaction();
            session.update(Vendedor);
            session.getTransaction().commit();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            session.getTransaction().rollback();
        } finally{
            if(session != null) {
                session.close();
            }
        }
    }

    @Override
    public void deleteVendedor(Vendedor vendedor) {
        Session session = null;
        try {
            session = Hibernateutil.getSessionFactory().openSession();
            session.beginTransaction();
            session.delete(Vendedor);
            session.getTransaction().commit();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            session.getTransaction().rollback();
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }
    
}
    
