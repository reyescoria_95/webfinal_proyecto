/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imp;

import dao.ProductoDAO;
import java.util.List;
import javax.jms.Session;
import javax.transaction.Transaction;

/**
 *
 * @author reyes
 */
public class ProductoDAOImp implements ProductoDAO {
    
    @Override
    public List<Producto> ListarProductos() {
        List<Producto> lista = null;
        Session session = HibernateUtil.getSessionfactory().openSession();
        Transaction t = session.beginTransaction();
        String hql="FROM Producto";
        
        try{
            lista = session.createQuery(hql).list();
            t.commit();
            session.close();
        }catch (Exception e) {
            System.out.println(e.getMessage());
            t.rollback();
        }
        return lista;
    }

    @Override
    public void newProducto(Producto producto) {
        Session session=null;
        try{
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.save(Producto); /*objeto completo*/
            session.getTransaction().commit();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            session.getTransaction().rollback();
        } finally {
            if(session != null) {
                session.close();
            }
        }
    }

    @Override
    public void updateProducto(Producto producto) {
        Session session=null;
        try{
            session = HibernateUtil.getsessionFactory().openSession();
            session.beginTransaction();
            session.update(Producto);
            session.getTransaction().commit();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            session.getTransaction().rollback();
        } finally{
            if(session != null) {
                session.close();
            }
        }
    }

    @Override
    public void deleteProducto(Producto producto) {
        Session session = null;
        try {
            session = Hibernateutil.getSessionFactory().openSession();
            session.beginTransaction();
            session.delete(Producto);
            session.getTransaction().commit();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            session.getTransaction().rollback();
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

}
